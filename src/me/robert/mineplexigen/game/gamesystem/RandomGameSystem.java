package me.robert.mineplexigen.game.gamesystem;

import java.util.Random;

import me.robert.mineplexigen.Mineplexigen;
import me.robert.mineplexigen.game.Game;

public class RandomGameSystem extends GameSystem{

	public RandomGameSystem(Mineplexigen engine) {
		super(engine);
	}

	@Override
	public Game getNextGame() {
		int random = new Random().nextInt(getGames().size());
		return getGames().get(random);	
	}

}
