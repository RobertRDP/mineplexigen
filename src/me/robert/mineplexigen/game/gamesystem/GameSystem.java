package me.robert.mineplexigen.game.gamesystem;

import java.util.ArrayList;

import me.robert.mineplexigen.Mineplexigen;
import me.robert.mineplexigen.game.Game;
import me.robert.mineplexigen.games.freemode.GameFreeMode;
import me.robert.mineplexigen.games.runner.GameRunner;
import me.robert.mineplexigen.games.spleef.GameSpleef;

public abstract class GameSystem {
	
	private Mineplexigen engine;
	private ArrayList<Game> games = new ArrayList<Game>();
	
	public GameSystem(Mineplexigen engine)
	{
		this.engine = engine;
		getGames().add(new GameRunner(getEngine()));
		getGames().add(new GameSpleef(getEngine()));
		getGames().add(new GameFreeMode(getEngine()));
	}
	
	public abstract Game getNextGame();
	
	public Mineplexigen getEngine()
	{
		return engine;
	}
	
	public ArrayList<Game> getGames()
	{
		return games;
	}
}
