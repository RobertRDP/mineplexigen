package me.robert.mineplexigen.lobby;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.WorldCreator;

import me.robert.mineplexigen.Mineplexigen;
import me.robert.mineplexigen.game.countdown.LobbyCountdown;
import me.robert.mineplexigen.lobby.module.LobbyJoinModule;
import me.robert.mineplexigen.lobby.module.LobbyLeaveModule;
import me.robert.mineplexigen.module.Module;
import me.robert.mineplexigen.module.common.AntiDamageSettingsModule;
import me.robert.mineplexigen.module.common.NoFoodChangeModule;

public class Lobby {
	
	private Mineplexigen engine;
	private Location spawn;
	private String worldName;
	private ArrayList<Module> modules = new ArrayList<Module>();
	private LobbyCountdown countdown;
	
	public Lobby(Mineplexigen engine, Location spawn, String worldName)
	{
		this.engine = engine;
		this.spawn = spawn;
		this.worldName = worldName;
		Bukkit.createWorld(new WorldCreator(worldName));
		addModule(new NoFoodChangeModule(engine));
		addModule(new LobbyJoinModule(engine));
		addModule(new LobbyLeaveModule(engine));
		addModule(new AntiDamageSettingsModule(engine));
		enableModules();
	}
	
	public void addModule(Module module)
	{
		modules.add(module);
	}
	
	public void enableModules()
	{
		for(Module module : modules)
		{
			module.onEnable();
		}
	}
	
	public void disableModules()
	{
		for(Module module : modules)
		{
			module.onDisable();
		}
	}
	
	public void startCountdown()
	{
		countdown = new LobbyCountdown(getEngine());
	}
	
	public Mineplexigen getEngine()
	{
		return engine;
	}
	
	public Location getSpawn()
	{
		return spawn;
	}
	
	public String getWorldName()
	{
		return worldName;
	}
		
	public LobbyCountdown getCountdown()
	{
		return countdown;
	}
}
