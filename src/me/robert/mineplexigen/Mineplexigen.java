package me.robert.mineplexigen;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import me.robert.mineplexigen.commands.GameCommand;
import me.robert.mineplexigen.commands.KitCommand;
import me.robert.mineplexigen.commands.ServerCommand;
import me.robert.mineplexigen.game.gamesystem.LinearGameSystem;
import me.robert.mineplexigen.games.GameManager;
import me.robert.mineplexigen.lobby.Lobby;
import me.robert.mineplexigen.rollback.RollbackHelper;
import me.robert.mineplexigen.scoreboard.ScoreboardManager;
import me.robert.mineplexigen.server.RankManager;
import me.robert.mineplexigen.server.SettingsListener;

public class Mineplexigen extends JavaPlugin {
	
	/**
	 * @author Robert 
	 */
	
	private Plugin plugin;
	private Lobby lobby;
	private GameManager gameManager;
	private ScoreboardManager scoreboardManager;
	private RollbackHelper rollbackHelper;
	private RankManager rankManager;
	
	@Override
	public void onEnable()
	{
		plugin = this;
		scoreboardManager = new ScoreboardManager();
		gameManager = new GameManager(this);
		rollbackHelper = new RollbackHelper(this);
		rankManager = new RankManager();
		getGameManager().setGameSystem(new LinearGameSystem(this));
		getGameManager().setNewGame();
		lobby = new Lobby(this, new Location(Bukkit.getWorld("lobby"), 12, 46, -23), "lobby");
		getCommand("game").setExecutor(new GameCommand(this));
		getCommand("kit").setExecutor(new KitCommand(this));
		getCommand("server").setExecutor(new ServerCommand(this));
		Bukkit.getPluginManager().registerEvents(new SettingsListener(this), this);
	}
	
	public Plugin getInstance()
	{
		return plugin;
	}
	
	public Lobby getLobby()
	{
		return lobby;
	}
	
	public GameManager getGameManager()
	{
		return gameManager;
	}
	
	public ScoreboardManager getScoreboardManager()
	{
		return scoreboardManager;
	}
	
	public RollbackHelper getRollbackHelper()
	{
		return rollbackHelper;
	}
	
	public RankManager getRankManager()
	{
		return rankManager;
	}

}
