package me.robert.mineplexigen.module.common;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.FoodLevelChangeEvent;

import me.robert.mineplexigen.Mineplexigen;
import me.robert.mineplexigen.module.Module;

public class NoFoodChangeModule extends Module {

	public NoFoodChangeModule(Mineplexigen engine) 
	{
		super(engine);
	}

	@Override
	public void onEnable() 
	{
		Bukkit.getPluginManager().registerEvents(this, getEngine());
	}

	@Override
	public void onDisable()
	{
		HandlerList.unregisterAll(this);
	}
	
	@EventHandler
	public void onFoodChange(FoodLevelChangeEvent event)
	{
		event.setCancelled(true);
	}

}
