package me.robert.mineplexigen.module.common;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerQuitEvent;

import me.robert.mineplexigen.Mineplexigen;
import me.robert.mineplexigen.game.GameSolo;
import me.robert.mineplexigen.module.Module;

public class PlayerQuitModule extends Module {
	
	private GameSolo game;

	public PlayerQuitModule(Mineplexigen engine, GameSolo game) 
	{
		super(engine);
		this.game = game;
	}

	@Override
	public void onEnable()
	{
		Bukkit.getPluginManager().registerEvents(this, getEngine());
	}

	@Override
	public void onDisable() 
	{
		HandlerList.unregisterAll(this);
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event)
	{
		if(game.getPlayers().contains(event.getPlayer()))
		{
		game.setDead(event.getPlayer());
		}
	}

}
