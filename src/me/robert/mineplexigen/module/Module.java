package me.robert.mineplexigen.module;

import org.bukkit.event.Listener;

import me.robert.mineplexigen.Mineplexigen;

public abstract class Module implements Listener {
	
	private Mineplexigen engine;
	
	public Module(Mineplexigen engine)
	{
		this.engine = engine;
	}
	
	public abstract void onEnable();
	public abstract void onDisable();
	
	public Mineplexigen getEngine()
	{
		return engine;
	}

}
