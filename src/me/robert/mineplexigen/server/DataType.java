package me.robert.mineplexigen.server;

public enum DataType {
	
	STRING,
	INT,
	BOOLEAN;

}
