package me.robert.mineplexigen.games.freemode;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import me.robert.mineplexigen.Mineplexigen;
import me.robert.mineplexigen.game.GameSolo;
import me.robert.mineplexigen.module.common.AntiDamageSettingsModule;

public class GameFreeMode extends GameSolo {

	public GameFreeMode(Mineplexigen engine) {
		super("FreeMode", "PARRTYY! FISSAA", 4, "freemode", engine);
		spawns[0] = new Location(Bukkit.getWorld(getWorldName()), 0, 0, 0);
		spawns[1] = new Location(Bukkit.getWorld(getWorldName()), 0, 0, 0);
		spawns[2] = new Location(Bukkit.getWorld(getWorldName()), 0, 0, 0);
		spawns[3] = new Location(Bukkit.getWorld(getWorldName()), 0, 0, 0);
		addModule(new AntiDamageSettingsModule(engine));
		
	}

}
