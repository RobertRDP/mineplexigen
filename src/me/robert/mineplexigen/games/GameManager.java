package me.robert.mineplexigen.games;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import me.robert.mineplexigen.Mineplexigen;
import me.robert.mineplexigen.game.Game;
import me.robert.mineplexigen.game.gamesystem.GameSystem;

public class GameManager {
	
	private Mineplexigen engine;
	private Game game;
	private GameSystem gameSystem;
	
	public GameManager(Mineplexigen engine)
	{
		this.engine = engine;
	}
	
	public void setNewGame()
	{
		setGame(gameSystem.getNextGame());
	}
	
	public void setGame(Game game)
	{
		this.game = game;
		for(Player players : Bukkit.getServer().getOnlinePlayers())
		{
			engine.getScoreboardManager().getScoreboard(players).editName(game.getGameTitle());
			engine.getScoreboardManager().getScoreboard(players).editLine(16, ChatColor.GRAY + "None");
		}
	}
	
	public Game getGame()
	{
		return game;
	}
	
	public void setGameSystem(GameSystem gameSystem)
	{
		this.gameSystem = gameSystem;
	}
	
	public GameSystem getGameSystem()
	{
		return gameSystem;
	}
}
