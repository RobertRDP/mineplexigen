package me.robert.mineplexigen.games.runner;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import me.robert.mineplexigen.Mineplexigen;
import me.robert.mineplexigen.game.GameSolo;
import me.robert.mineplexigen.games.runner.kit.RunnerLeaperKit;
import me.robert.mineplexigen.games.runner.kit.RunnerRabbitKit;
import me.robert.mineplexigen.games.runner.module.RunnerAntiDamageModule;
import me.robert.mineplexigen.games.runner.module.RunnerBlockDropModule;
import me.robert.mineplexigen.games.runner.module.RunnerDeathModule;
import me.robert.mineplexigen.module.common.NoFoodChangeModule;

public class GameRunner extends GameSolo{

	public GameRunner(Mineplexigen engine) 
	{
		super("Runner", "Run, the blocks will fall under your feet!", 4, "runner", engine);
		spawns[0] = new Location(Bukkit.getWorld(getWorldName()), -9, 84, -13);
		spawns[1] = new Location(Bukkit.getWorld(getWorldName()), -20, 84, 1);
		spawns[2] = new Location(Bukkit.getWorld(getWorldName()), 2, 84, 1);
		spawns[3] = new Location(Bukkit.getWorld(getWorldName()), -9, 84, 16);
		addModule(new RunnerAntiDamageModule(engine));
		addModule(new RunnerDeathModule(engine, this));
		addModule(new NoFoodChangeModule(engine));
		addModule(new RunnerBlockDropModule(engine));
		addKit(new RunnerLeaperKit());
		addKit(new RunnerRabbitKit());
	}

}
