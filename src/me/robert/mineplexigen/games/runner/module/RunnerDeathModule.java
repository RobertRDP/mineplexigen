package me.robert.mineplexigen.games.runner.module;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerMoveEvent;

import me.robert.mineplexigen.Mineplexigen;
import me.robert.mineplexigen.game.GameSolo;
import me.robert.mineplexigen.module.Module;

public class RunnerDeathModule extends Module {
	
	private GameSolo game;

	public RunnerDeathModule(Mineplexigen engine, GameSolo game) 
	{
		super(engine);
		this.game = game;
	}

	@Override
	public void onEnable() 
	{
		Bukkit.getPluginManager().registerEvents(this, getEngine());
	}

	@Override
	public void onDisable() 
	{
		HandlerList.unregisterAll(this);
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerMoveEvent event)
	{
		if(event.getPlayer().getLocation().getY() < 60)
		{
			game.setDead(event.getPlayer());
		}
	}

}
