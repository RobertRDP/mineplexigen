package me.robert.mineplexigen.games.runner.module;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageEvent;

import me.robert.mineplexigen.Mineplexigen;
import me.robert.mineplexigen.module.Module;

public class RunnerAntiDamageModule extends Module {

	public RunnerAntiDamageModule(Mineplexigen engine) 
	{
		super(engine);
	}

	@Override
	public void onEnable() 
	{
		Bukkit.getPluginManager().registerEvents(this, getEngine());
	}

	@Override
	public void onDisable() 
	{
		HandlerList.unregisterAll(this);
	}
	
	@EventHandler
	public void onDamage(EntityDamageEvent event)
	{
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onBlockbreak(BlockBreakEvent event) 
	{
			  event.setCancelled(true);
	}

}
