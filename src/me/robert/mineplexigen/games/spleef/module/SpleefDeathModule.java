package me.robert.mineplexigen.games.spleef.module;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerMoveEvent;

import me.robert.mineplexigen.Mineplexigen;
import me.robert.mineplexigen.game.GameSolo;
import me.robert.mineplexigen.module.Module;

public class SpleefDeathModule extends Module {
	
	private GameSolo game;

	public SpleefDeathModule(Mineplexigen engine, GameSolo game) 
	{
		super(engine);
		this.game = game;
	}

	@Override
	public void onEnable() 
	{
		Bukkit.getPluginManager().registerEvents(this, getEngine());
	}

	@Override
	public void onDisable() 
	{
		HandlerList.unregisterAll(this);
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerMoveEvent event)
	{
		if(event.getPlayer().getLocation().getY() < 50 && game.getPlayers().contains(event.getPlayer()))
		{
			game.setDead(event.getPlayer());
		}
	}

}
