package me.robert.mineplexigen.games.spleef;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import me.robert.mineplexigen.Mineplexigen;
import me.robert.mineplexigen.game.GameSolo;
import me.robert.mineplexigen.games.spleef.kit.SpleefFreezerKit;
import me.robert.mineplexigen.games.spleef.kit.SpleefLeaperKit;
import me.robert.mineplexigen.games.spleef.module.SpleefDamageModule;
import me.robert.mineplexigen.games.spleef.module.SpleefDeathModule;
import me.robert.mineplexigen.games.spleef.module.SpleefInteractModule;
import me.robert.mineplexigen.module.common.NoFoodChangeModule;

public class GameSpleef extends GameSolo {
	
	public GameSpleef(Mineplexigen engine)
	{
		super("Spleef", "Break blocks to make your enemy fall!", 4, "spleef", engine);
		spawns[0] = new Location(Bukkit.getWorld(getWorldName()), 6, 63, -76);
		spawns[1] = new Location(Bukkit.getWorld(getWorldName()), -4, 63, -86);
		spawns[2] = new Location(Bukkit.getWorld(getWorldName()), 7, 63, -96);
		spawns[3] = new Location(Bukkit.getWorld(getWorldName()), 17, 63, -86);
		addModule(new SpleefInteractModule(engine));
		addModule(new SpleefDeathModule(engine, this));
		addModule(new SpleefDamageModule(engine));
		addModule(new NoFoodChangeModule(engine));
		addKit(new SpleefLeaperKit());
		addKit(new SpleefFreezerKit());
	}
}
